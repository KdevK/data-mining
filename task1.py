import numpy

L = numpy.array(
    [
     [0, 1, 0, 0, 0, 0],
     [0, 0, 1, 0, 0, 0],
     [0, 0, 0, 0, 0, 0],
     [0, 0, 1, 0, 1, 0],
     [1, 1, 0, 0, 0, 0],
     [0, 0, 0, 1, 0, 0],
    ]
)

LT = L.T

h = numpy.array([1, 1, 1, 1, 1, 1]).reshape((6, 1))
a = numpy.array([1, 1, 1, 1, 1, 1]).reshape((6, 1))
la = numpy.array([1, 1, 1, 1, 1, 1]).reshape((6, 1))


for i in range(10):
    h = la / (numpy.max(la))
    lth = LT @ h
    a = lth / (numpy.max(lth))
    la = L @ a


print('habbiness info:')
for i, k in enumerate(h):
    print(f'{i+1}: {round(k[0], 3)}')


print('authority info:')
for i, k in enumerate(a):
    print(f'{i+1}: {round(k[0], 3)}')


# ANSWER
"""habbiness info:
1: 0.618
2: 0.618
3: 0.0
4: 1.0
5: 1.0
6: 0.0
authority info:
1: 0.618
2: 1.0
3: 1.0
4: 0.0
5: 0.618
6: 0.0
"""
