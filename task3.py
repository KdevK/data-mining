transactions = {}
product_set = []
items_count = {}
trans_pairs = {}
prod_to_num = {}
num_to_prod = {}
hash_buckets_1 = {}
hash_buckets_2 = {}
to_ignore = []

support = 4


# split array [a, b, c, d] into array of arrays [[a, b], [a, c], [a, d], [b, c], [b, d], [c, d]]
def to_pairs(array: list):
    ans = []
    if len(array) != 1:
        for i in range(len(array)):
            for j in range(i+1, len(array)):
                ans.append([array[i], array[j]])
        return ans


def hash_func_1(x, y, mod):
    return (17*x**y+y) % 300000000


def hash_func_2(x, y, mod):
    return (42*x+28*y**x) % 30000000


# read csv file, create a dict of transactions and fill the products set
with open('transactions.csv', 'r') as file:
    for line in file:
        new_line = line.split(';')
        if new_line[0] == 'PROD_CODE':
            continue
        product_set.append(new_line[0].strip())
        if new_line[1].strip() not in transactions:
            transactions[new_line[1].strip()] = []
        transactions[new_line[1].strip()].append(new_line[0])
    product_set = set(product_set)


# map product names with numbers
for i, product in enumerate(product_set):
    prod_to_num[product] = i + 1
    num_to_prod[i+1] = product

# turn product names into numbers
for i in transactions:
    for j in range(len(transactions[i])):
        transactions[i][j] = prod_to_num[transactions[i][j]]


# count the products
for customer in transactions:
    products = transactions[customer]

    for item in products:
        if item not in items_count:
            items_count[item] = 0
        items_count[item] += 1


# make a dictionary with products split into pairs
for customer in transactions:
    if len(transactions[customer]) != 1:
        trans_pairs[customer] = to_pairs(transactions[customer])

# first pass hash
for customer in trans_pairs:
    for pair in trans_pairs[customer]:
        if (hash_func_1(pair[0], pair[1], len(items_count))) not in hash_buckets_1:
            hash_buckets_1[hash_func_1(pair[0], pair[1], len(items_count))] = []
        hash_buckets_1[hash_func_1(pair[0], pair[1], len(items_count))].append(pair)

print('len is'+str(len(hash_buckets_1)))
for i in hash_buckets_1:
    print(len(hash_buckets_1[i]))

# create an array with the items that should not be counted
for key in hash_buckets_1:
    if 0 < len(hash_buckets_1[key]) < support:
        for pair in hash_buckets_1[key]:
            if pair not in to_ignore:
                to_ignore.append(pair)

count = 0
# filter items
for customer in trans_pairs:
    for pair in trans_pairs[customer]:
        if pair in to_ignore:
            trans_pairs[customer].remove(pair)
            count += 1
            print(f'{pair} is removed')

print(f'Removed {count} doubletons')


# second pass hash
for customer in trans_pairs:
    for pair in trans_pairs[customer]:
        if (hash_func_1(pair[0], pair[1], len(items_count))) not in hash_buckets_2:
            hash_buckets_2[hash_func_1(pair[0], pair[1], len(items_count))] = []
        hash_buckets_2[hash_func_1(pair[0], pair[1], len(items_count))].append(pair)


to_ignore = []
# create an array with the items that should not be counted
for key in hash_buckets_2:
    if 0 < len(hash_buckets_2[key]) < support:
        for pair in hash_buckets_2[key]:
            if pair not in to_ignore:
                to_ignore.append(pair)

count = 0
# filter items
for customer in trans_pairs:
    for pair in trans_pairs[customer]:
        if pair in to_ignore:
            trans_pairs[customer].remove(pair)
            count += 1
            print(f'{pair} is removed')

print(f'Removed {count} doubletons')

to_ignore = []

# create an array with the singletons that should not be counted
for product in items_count:
    if items_count[product] < support and product not in to_ignore:
        to_ignore.append(product)

# filter items
for customer in trans_pairs:
    for pair in trans_pairs[customer]:
        if pair[0] in to_ignore or pair[1] in to_ignore:
            trans_pairs[customer].remove(pair)
            continue

# append frequent pairs to the answer array
answer = []
for customer in trans_pairs:
    for pair in trans_pairs[customer]:
        answer.append(pair)


# translate product ids to product names
for i in range(len(answer)):
    answer[i][0] = num_to_prod[answer[i][0]]
    answer[i][1] = num_to_prod[answer[i][1]]


answer = list(set(tuple(x) for x in answer))

# append singletons to the answer awway
for product in items_count:
    if items_count[product] >= support:
        answer.append(num_to_prod[product])


# write down the answer values into the txt file
with open('answer.txt', 'w') as output:
    for i in answer:
        output.write(str(i)+'\n')
