import re
import math
from zlib import adler32

with open('text.txt', encoding='utf-8') as f1, open('word_list.txt', 'w', encoding='utf-8') as f2:
    f2.write('\n'.join(set(re.findall('[а-яА-Я]+', f1.read()))))

p = 0.1  # desired false positive probability

word_set = []  # list of unique words from the text
bit_array = []  # an array which contains the results of hash functions


# function that returns hashed values depending on the given seed
def hash_f(seed, value: str):
    value = adler32(value.encode())
    return value ** (seed+1)


# filling the word set with unique words
with open('word_list.txt', encoding='utf-8') as f:
    for line in f:
        word_set.append(line.strip())

n = len(word_set)  # length of the word set

m = round(-n * math.log(p) / (math.log(2) ** 2))  # size of bit array

bit_array = [0 for i in range(m)]

print(f'Bit array length: {m}')

k = math.ceil(m / n * math.log(2))  # optimal number of hash functions

print(f'The number of hash functions: {k}')


# filling the bit array
for word in word_set:
    for i in range(k):
        bit_array[hash_f(i, word) % m] += 1

print(bit_array)

# main loop
while True:
    search = input('type your word:\n')

    flag = True
    for i in range(k):
        if bit_array[hash_f(i, search) % m] == 0:
            print('totally no!')
            flag = False
            break

    if flag:
        print('maybe')
